import unittest
from kadane import *
import requests
from unittest.mock import patch


class KadaneTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Starting testing for MyTestCase class")

    @classmethod
    def tearDownClass(cls):
        print("Tearing down the MyTestCase class")

    def setUp(self):
        print()
        print("Starting another test....")

    def tearDown(self):
        print("Tear down this test....")

    def test_all_positive_elements(self):
        print("All positive elements test")
        obj = Kadane()
        arr = [2, 3, 6]
        result = obj.maxSubArraySum(arr, len(arr))
        self.assertEqual(result, 11)

    def test_all_negative_elements(self):
        print("All negative elements test")
        obj = Kadane()
        arr = [-2, -199, -68, -1, -3]
        result = obj.maxSubArraySum(arr, len(arr))
        self.assertEqual(result, -1)

    def test_positive_negative_zero(self):
        print("mixture elements test")
        obj = Kadane()
        arr = [4, -5, 0, 2, 8, -2, 3]
        result = obj.maxSubArraySum(arr, len(arr))
        self.assertEqual(result, 11)

    def test_size_int(self):
        print("size int test")
        obj = Kadane()
        arr = [4, -5, 0, 2, 8, -2, 3]
        with self.assertRaises(TypeError):
            obj.maxSubArraySum(arr, 'seven')

    def test_elements_int(self):
        print("Elements are integer test")
        obj = Kadane()
        arr = [4, -5, 0, 2, '8', -2, 3]
        with self.assertRaises(TypeError):
            obj.maxSubArraySum(arr, 7)

    def test_code_link(self):
        print("code link test")
        obj = Kadane()
        with patch('requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Code is there.'

            url = 'https://www.geeksforgeeks.org/largest-sum-contiguous-subarray/'
            res = obj.code_link(url)
            mocked_get.assert_called_with(url)
            self.assertEqual(res, 'Code is there.')

            mocked_get.return_value.ok = False
            res = obj.code_link(url)
            mocked_get.assert_called_with(url)
            self.assertEqual(res, 'Bad Response!')


if __name__ == '__main__':
    unittest.main()
